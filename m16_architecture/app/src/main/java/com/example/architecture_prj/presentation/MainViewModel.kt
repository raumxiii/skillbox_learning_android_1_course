package com.example.architecture_prj.presentation

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.architecture_prj.domain.GetUsefulActivityUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

private const val VIEW_MODEL_LOG_TAG = "VIEW_MODEL_LOG"

@HiltViewModel
class MainViewModel @Inject constructor(private val activityUseCase: GetUsefulActivityUseCase) :
    ViewModel() {
    private val _state = MutableStateFlow<State>(State.ReadyForSearch)
    val state = _state.asStateFlow()

    fun reloadUsefulActivity() {
        try {
            _state.value = State.LoadingActivity
            viewModelScope.launch {
                try {
                    val usefulActivity = activityUseCase.execute()
                    if (usefulActivity == null)
                        _state.value = State.ErrorNoActivity
                    else
                        _state.value = State.ActivityLoaded(usefulActivity)
                } catch (e: Throwable) {
                    Log.e(
                        VIEW_MODEL_LOG_TAG,
                        "$e \r\n cause: ${e.cause.toString()} \r\n suppressed by: ${e.suppressed}"
                    )
                    _state.value = State.MainError("$e")
                }
            }
        } catch (e: Throwable) {
            Log.e(
                VIEW_MODEL_LOG_TAG,
                "$e \r\n cause: ${e.cause.toString()} \r\n suppressed by: ${e.suppressed}"
            )
            _state.value = State.MainError("$e")
        } finally {
            _state.value = State.ReadyForSearch
        }
    }
}