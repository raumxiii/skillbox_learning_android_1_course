package com.example.architecture_prj.domain

import com.example.architecture_prj.data.UsefulActivitiesRepository
import com.example.architecture_prj.entity.UsefulActivity
import javax.inject.Inject

class GetUsefulActivityUseCase @Inject constructor(private val repository: UsefulActivitiesRepository) {
    suspend fun execute(): UsefulActivity? {
        return repository.getUsefulActivity()
    }
}