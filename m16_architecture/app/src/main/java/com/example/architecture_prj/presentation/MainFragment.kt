package com.example.architecture_prj.presentation

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.example.architecture_prj.R
import com.example.architecture_prj.databinding.FragmentMainBinding
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainFragment : Fragment() {
    companion object {
        fun newInstance() = MainFragment()
    }

    private val viewModel: MainViewModel by viewModels()
    private lateinit var binding: FragmentMainBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentMainBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.refreshButton.setOnClickListener {
            viewModel.reloadUsefulActivity()
        }

        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewModel.state.collect { state ->
                when (state) {
                    State.LoadingActivity -> {
                        binding.refreshButton.isEnabled = false
                        binding.usefulActivityTextView.text = ""
                    }
                    State.ReadyForSearch -> {
                        binding.refreshButton.isEnabled = true
                    }
                    State.ErrorNoActivity -> {
                        binding.usefulActivityTextView.text =
                            resources.getString(R.string.useful_activity_default_text)
                    }
                    is State.ActivityLoaded -> {
                        binding.usefulActivityTextView.text = state.activity.activity
                    }
                    is State.MainError -> {
                        Snackbar.make(view, state.message, Snackbar.LENGTH_LONG).show()
                    }
                }
            }
        }
    }
}