package com.example.architecture_prj.presentation

import com.example.architecture_prj.entity.UsefulActivity

sealed class State {
    object ReadyForSearch : State()
    object LoadingActivity : State()
    object ErrorNoActivity : State()
    data class ActivityLoaded(val activity: UsefulActivity) : State()
    data class MainError(val message: String) : State()
}
