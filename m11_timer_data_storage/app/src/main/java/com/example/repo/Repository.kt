package com.example.repo

import android.content.Context
import android.content.Context.MODE_PRIVATE

private const val SHARED_PREFERENCE = "SHARED_PREFERENCE_NAME"
private const val TEXT_VALUE_SH_PREFS_KEY = "TEXT_VALUE"

class Repository(context: Context, private val noFoundValue: String) {
    private var textValue: String? = null
    private val shPref = context.getSharedPreferences(SHARED_PREFERENCE, MODE_PRIVATE)
    private val shPrefEditor = shPref.edit()

    private fun getDataFromSharedPreference(): String? {
        return shPref.getString(TEXT_VALUE_SH_PREFS_KEY, null)
    }

    private fun getDataFromLocalVariable(): String? {
        return textValue
    }

    fun saveText(text: String) {
        textValue = text
        shPrefEditor.putString(TEXT_VALUE_SH_PREFS_KEY, text)
        shPrefEditor.apply()
    }

    fun clearText() {
        textValue = null
        shPrefEditor.remove(TEXT_VALUE_SH_PREFS_KEY)
        shPrefEditor.apply()
    }

    fun getText(): String {
        return when {
            getDataFromLocalVariable() != null -> getDataFromLocalVariable()!!
            getDataFromSharedPreference() != null -> getDataFromSharedPreference()!!
            else -> noFoundValue
        }
    }
}