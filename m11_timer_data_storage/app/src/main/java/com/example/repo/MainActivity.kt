package com.example.repo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.repo.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val repository = Repository(this, resources.getString(R.string.no_found_value))

        binding.infoTextView.text = repository.getText()

        binding.clearButton.setOnClickListener {
            repository.clearText()
            binding.infoTextView.text = repository.getText()
        }

        binding.saveButton.setOnClickListener {
            when (binding.infoEditText.text.length) {
                0 -> repository.clearText()
                else -> repository.saveText(binding.infoEditText.text.toString())
            }
            binding.infoTextView.text = repository.getText()
        }
    }
}