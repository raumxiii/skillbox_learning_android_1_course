package com.example.prj

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.example.prj.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private var passengerCount: Int = 0
    private lateinit var binding: ActivityMainBinding
    private var busCapacity: Int = 0

    private fun refreshInterface() {
        when {
            passengerCount == 0 -> {
                binding.buttonMinus.isEnabled = false
                binding.buttonPlus.isEnabled = true
                binding.buttonRelease.visibility = View.INVISIBLE
                binding.busStatusText.setTextAppearance(R.style.InfoTextEmptyBusTheme)
                binding.busStatusText.text = resources.getString(R.string.bus_empty_text)
            }
            passengerCount >= busCapacity -> {
                binding.buttonMinus.isEnabled = true
                binding.buttonPlus.isEnabled = false
                binding.buttonRelease.visibility = View.VISIBLE
                binding.busStatusText.setTextAppearance(R.style.InfoTextOverloadBusTheme)
                binding.busStatusText.text = resources.getString(R.string.bus_overload_text)
            }
            else -> {
                binding.buttonMinus.isEnabled = true
                binding.buttonPlus.isEnabled = true
                binding.buttonRelease.visibility = View.INVISIBLE
                binding.busStatusText.setTextAppearance(R.style.InfoTextHavePlaceBusTheme)
                binding.busStatusText.text = resources.getString(R.string.bus_have_place_text)
                    .format((busCapacity - passengerCount).toString())
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)

        setContentView(binding.root)
        busCapacity = resources.getInteger(R.integer.bus_capacity)

        refreshInterface()

        binding.buttonPlus.setOnClickListener {
            passengerCount++
            refreshInterface()
        }
        binding.buttonMinus.setOnClickListener {
            passengerCount--
            refreshInterface()
        }
        binding.buttonRelease.setOnClickListener {
            passengerCount = 0
            refreshInterface()
        }
    }


}