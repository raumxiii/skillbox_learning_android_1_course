package com.example.searchit.ui.main

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.example.searchit.R

import com.example.searchit.databinding.FragmentMainBinding

class MainFragment : Fragment() {
    private lateinit var binding: FragmentMainBinding

    companion object {
        fun newInstance() = MainFragment()
    }

    private val viewModel: MainViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentMainBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.searchButton.setOnClickListener {
            val searchString: String = binding.searchInputEditText.text.toString()
            viewModel.onSearchButtonClick(searchString)
        }

        binding.searchInputEditText.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                viewModel.onSearchEdit(binding.searchInputEditText.text.toString())
            }

            override fun afterTextChanged(s: Editable?) {}
        })
        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewModel.state.collect { state ->
                when (state) {
                    State.Searching -> {
                        binding.searchButton.isEnabled = false
                        binding.progressBar.isEnabled = true
                        binding.progressBar.isVisible = true
                        binding.message.text = ""
                    }
                    is State.Success -> {
                        binding.searchButton.isEnabled = true
                        binding.progressBar.isEnabled = false
                        binding.progressBar.isVisible = false
                        binding.message.text = state.resultText
                    }
                    State.ReadyToSearch -> {
                        binding.searchButton.isEnabled = true
                        binding.searchInputLayout.setError(null)
                    }
                    State.ShortSearchString -> {
                        binding.searchButton.isEnabled = false
                        binding.searchInputLayout.setError(resources.getString(R.string.too_low_search_string_text))
                    }
                }
            }
        }
    }

}
