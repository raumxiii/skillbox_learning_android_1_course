package com.example.searchit.ui.main

sealed class State {
    object Searching : State()
    object ReadyToSearch : State()
    object ShortSearchString : State()
    data class Success(val resultText: String) : State()
}
