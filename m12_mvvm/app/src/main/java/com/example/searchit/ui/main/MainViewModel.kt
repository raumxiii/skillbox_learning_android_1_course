package com.example.searchit.ui.main

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

class MainViewModel : ViewModel() {
    private val _state = MutableStateFlow<State>(State.ShortSearchString)
    val state = _state.asStateFlow()

    fun onSearchButtonClick(searchString: String) {
        viewModelScope.launch {
            _state.value = State.Searching
            delay(5000)
            _state.value =
                State.Success("По запросу <$searchString> ничего не найдено - в следующий раз что-нибудь найдется, Обязательно!")
        }
    }

    fun onSearchEdit(searchString: String) {
        if (searchString.length < 3) {
            _state.value = State.ShortSearchString
        } else {
            _state.value = State.ReadyToSearch
        }
    }
}