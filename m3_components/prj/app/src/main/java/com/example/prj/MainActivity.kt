package com.example.prj

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.SeekBar
import android.widget.Toast
import com.example.prj.databinding.ActivityMainBinding
import kotlinx.coroutines.*

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private var periodValue = 0
    private var remainPeriodValue = 0
    private var isInProgress = false
    private var mainCoroutineScope = CoroutineScope(Dispatchers.Main)
    private var mainJob: Job = Job()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        mainJob.cancel()

        binding.timePickerSeekBar.setOnSeekBarChangeListener(object :
            SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(SeekBar: SeekBar, value: Int, fromUser: Boolean) {
                remainPeriodValue = value
                refreshValues()
            }

            override fun onStartTrackingTouch(p0: SeekBar) {}
            override fun onStopTrackingTouch(p0: SeekBar) {}
        })

        binding.timePickerSlider.addOnChangeListener { _, value, _ ->
            remainPeriodValue = value.toInt()
            refreshValues()
        }

        binding.button.setOnClickListener {
            if (!isInProgress) {
                run()
            } else stop()
        }
    }

    private fun refreshValues() {
        binding.timePickerSeekBar.progress = remainPeriodValue
        binding.timePickerSlider.value = remainPeriodValue.toFloat()
        binding.timeRemainTextView.text = remainPeriodValue.toString()
        if (isInProgress && periodValue > 0) {
            binding.progressBarMain.progress =
                ((1.0f - remainPeriodValue.toFloat() / periodValue.toFloat()) * 100).toInt()
        } else
            binding.progressBarMain.progress = 0
    }

    private fun run() {
        if (mainJob.isActive || remainPeriodValue <= 0)
            return

        periodValue = remainPeriodValue

        binding.timePickerSlider.isEnabled = false
        binding.timePickerSeekBar.isEnabled = false
        binding.button.text = resources.getString(R.string.button_in_progress_text)
        Toast.makeText(
            this,
            resources.getString(R.string.progress_start_text).format(periodValue.toString()),
            Toast.LENGTH_SHORT
        ).show()

        isInProgress = true

        mainJob = mainCoroutineScope.launch {
            while (remainPeriodValue > 0) {
                delay(1000)
                remainPeriodValue--
                refreshValues()
            }
            stop()
        }
    }

    private fun stop() {
        if (mainJob.isActive) {
            mainJob.cancel()
        }
        if (remainPeriodValue == 0)
            Toast.makeText(
                this,
                resources.getString(R.string.progress_end_text),
                Toast.LENGTH_SHORT
            ).show()
        else
            Toast.makeText(
                this,
                resources.getString(R.string.progress_stop_text)
                    .format(remainPeriodValue.toString()),
                Toast.LENGTH_SHORT
            ).show()

        binding.timePickerSlider.isEnabled = true
        binding.timePickerSeekBar.isEnabled = true
        binding.button.text = resources.getString(R.string.button_text)
        isInProgress = false
        periodValue = 0
        refreshValues()
    }
}