package com.example.skillbox_hw_quiz.ui.main

import android.content.res.Resources
import android.os.Bundle
import android.transition.TransitionInflater
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.navigation.fragment.findNavController
import com.example.skillbox_hw_quiz.R
import com.example.skillbox_hw_quiz.databinding.FragmentQuestionBinding
import com.example.skillbox_hw_quiz.quiz.Quiz
import com.example.skillbox_hw_quiz.quiz.QuizStorage
import java.util.*


class QuestionFragment : Fragment() {
    private lateinit var quiz: Quiz
    private var _binding: FragmentQuestionBinding? = null
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val inflater = TransitionInflater.from(requireContext())
        enterTransition = inflater.inflateTransition(R.transition.form_slide_right)
        exitTransition = inflater.inflateTransition(R.transition.form_slide_down)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentQuestionBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        buttonAppear(
            binding.backButton,
            resources.getInteger(R.integer.form_button_appear_duration)
        )
        buttonAppear(
            binding.confirmAnswerButton,
            resources.getInteger(R.integer.form_button_appear_duration)
        )

        val currentLocale = Resources.getSystem().getConfiguration().locales.get(0)
        quiz = QuizStorage.getQuiz(
            when (currentLocale.language) {
                "ru" -> QuizStorage.Locale.Ru
                else -> QuizStorage.Locale.En
            }
        )
        fillForm()

        binding.confirmAnswerButton.setOnClickListener {
            val answerList: List<Int> = readAnswerList()
            val answerFeedback = QuizStorage.answer(quiz, answerList)
            val bundle = Bundle().apply { putString("param1", answerFeedback) }
            findNavController().navigate(
                R.id.action_questionFragment_to_resultViewFragment,
                args = bundle
            )
        }
        binding.backButton.setOnClickListener { findNavController().navigate(R.id.action_questionFragment_to_mainFragment) }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun fillForm() {
        binding.question0TextView.setText(quiz.questions[0].question)
        binding.question1TextView.setText(quiz.questions[1].question)
        binding.question2TextView.setText(quiz.questions[2].question)

        binding.radioButton00.setText(quiz.questions[0].answers[0])
        binding.radioButton01.setText(quiz.questions[0].answers[1])
        binding.radioButton02.setText(quiz.questions[0].answers[2])
        binding.radioButton03.setText(quiz.questions[0].answers[3])

        binding.radioButton10.setText(quiz.questions[1].answers[0])
        binding.radioButton11.setText(quiz.questions[1].answers[1])
        binding.radioButton12.setText(quiz.questions[1].answers[2])
        binding.radioButton13.setText(quiz.questions[1].answers[3])

        binding.radioButton20.setText(quiz.questions[2].answers[0])
        binding.radioButton21.setText(quiz.questions[2].answers[1])
        binding.radioButton22.setText(quiz.questions[2].answers[2])
        binding.radioButton23.setText(quiz.questions[2].answers[3])
    }

    private fun readAnswerList(): List<Int> {
        val answerList: MutableList<Int> = mutableListOf()
        var answerIndex: Int
        answerIndex = when {
            binding.radioButton00.isChecked -> 0
            binding.radioButton01.isChecked -> 1
            binding.radioButton02.isChecked -> 2
            binding.radioButton03.isChecked -> 3
            else -> -1
        }
        answerList.add(0, answerIndex)

        answerIndex = when {
            binding.radioButton10.isChecked -> 0
            binding.radioButton11.isChecked -> 1
            binding.radioButton12.isChecked -> 2
            binding.radioButton13.isChecked -> 3
            else -> -1
        }
        answerList.add(1, answerIndex)

        answerIndex = when {
            binding.radioButton20.isChecked -> 0
            binding.radioButton21.isChecked -> 1
            binding.radioButton22.isChecked -> 2
            binding.radioButton23.isChecked -> 3
            else -> -1
        }
        answerList.add(2, answerIndex)

        return answerList
    }

    private fun buttonAppear(button: Button, dur: Int) {
        println("${button.text}, $dur")
        button.alpha = 0f
        button.animate().apply {
            duration = dur.toLong()
            alpha(1f)
        }.start()
    }

    companion object {
        @JvmStatic
        fun newInstance() = QuestionFragment().apply { }
    }
}

