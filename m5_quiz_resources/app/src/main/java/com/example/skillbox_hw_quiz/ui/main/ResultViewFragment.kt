package com.example.skillbox_hw_quiz.ui.main

import android.animation.AnimatorInflater
import android.animation.AnimatorSet
import android.os.Bundle
import android.transition.TransitionInflater
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.example.skillbox_hw_quiz.R
import com.example.skillbox_hw_quiz.databinding.FragmentResultViewBinding

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"

/**
 * A simple [Fragment] subclass.
 * Use the [ResultViewFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ResultViewFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null

    private var _binding: FragmentResultViewBinding? = null
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
        }
        val inflater = TransitionInflater.from(requireContext())
        enterTransition = inflater.inflateTransition(R.transition.form_slide_up)
        exitTransition = inflater.inflateTransition(R.transition.form_slide_down)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentResultViewBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val toStartButton = binding.toStartButton

        binding.resultTextView.setText(param1)
        toStartButton.setOnClickListener { findNavController().navigate(R.id.action_resultViewFragment_to_mainFragment) }
        (AnimatorInflater.loadAnimator(
            requireContext(), R.animator.button_rotate_textcolor_animation
        ) as AnimatorSet).apply {
            setTarget(toStartButton)
            start()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @return A new instance of fragment ResultViewFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String) =
            ResultViewFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                }
            }
    }
}