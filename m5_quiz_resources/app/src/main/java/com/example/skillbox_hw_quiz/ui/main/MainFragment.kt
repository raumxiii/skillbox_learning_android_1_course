package com.example.skillbox_hw_quiz.ui.main

import android.animation.ObjectAnimator
import android.content.res.Resources
import android.icu.text.SimpleDateFormat
import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.transition.TransitionInflater
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AccelerateDecelerateInterpolator
import androidx.navigation.fragment.findNavController
import com.example.skillbox_hw_quiz.R
import com.example.skillbox_hw_quiz.databinding.MainFragmentBinding
import com.google.android.material.datepicker.CalendarConstraints
import com.google.android.material.datepicker.MaterialDatePicker
import com.google.android.material.snackbar.Snackbar
import java.util.*

class MainFragment : Fragment() {

    companion object {
        fun newInstance() = MainFragment()
    }

    private lateinit var viewModel: MainViewModel
    private var _binding: MainFragmentBinding? = null
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val inflater = TransitionInflater.from(requireContext())
        enterTransition = inflater.inflateTransition(R.transition.form_slide_up)
        exitTransition = inflater.inflateTransition(R.transition.form_slide_left)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = MainFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        var birthDate: Calendar = Calendar.getInstance()
        val dateFormat = SimpleDateFormat(resources.getString(R.string.date_format))

        binding.startButton.setOnClickListener { findNavController().navigate(R.id.action_mainFragment_to_questionFragment) }
        ObjectAnimator.ofFloat(binding.startButton, View.ROTATION_Y, 0f, 360f)
            .apply {
                duration = resources.getInteger(R.integer.button_rotate_duration).toLong()
                repeatCount = ObjectAnimator.INFINITE
                interpolator = AccelerateDecelerateInterpolator()
                start()
            }

        binding.getBirthdayButton.setOnClickListener {
            val calendarConstraints =
                CalendarConstraints.Builder().setOpenAt(birthDate.timeInMillis).build()

            val getDateDialog = MaterialDatePicker.Builder.datePicker()
                .setTitleText(resources.getString(R.string.get_birthday_title))
                .setSelection(birthDate.timeInMillis)
                .setCalendarConstraints(calendarConstraints)
                .build()
            getDateDialog.addOnPositiveButtonClickListener { timeMillis ->
                birthDate.timeInMillis = timeMillis

                Snackbar.make(
                    binding.getBirthdayButton,
                    dateFormat.format(birthDate.time),
                    Snackbar.LENGTH_LONG
                ).show()
            }
            getDateDialog.show(childFragmentManager, "BirthdayPicker")
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(MainViewModel::class.java)
        // TODO: Use the ViewModel
    }

}