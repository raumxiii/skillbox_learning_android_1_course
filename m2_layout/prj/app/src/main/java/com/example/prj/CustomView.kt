package com.example.prj

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.LinearLayout
import com.example.prj.databinding.MyCustomViewBinding

class CustomView
@JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : LinearLayout(context, attrs, defStyleAttr) {
    private val binding = MyCustomViewBinding.inflate(LayoutInflater.from(context))

    init {
        addView(binding.root)
    }

    fun setTopText(textVal: String) {
        binding.bottomTextView.text = textVal
    }

    fun setBottomText(textVal: String) {
        binding.topTextView.text = textVal
    }
}