package com.example.prj

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.prj.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.mainCustomView.setBottomText(resources.getString(R.string.bottom_text))
        binding.mainCustomView.setTopText(resources.getString(R.string.top_text))
    }
}