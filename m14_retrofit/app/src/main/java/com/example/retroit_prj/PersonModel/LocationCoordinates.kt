package com.example.retroit_prj.PersonModel

import com.google.gson.annotations.SerializedName

data class LocationCoordinates(
    @SerializedName("latitude") val latitude: String,
    @SerializedName("longitude") val longitude: String
)
