package com.example.retroit_prj

import com.example.retroit_prj.PersonModel.PersonModel

sealed class State {
    object LoadingPerson : State()
    object LoadingPicture : State()
    object ReadyForSearch : State()
    object ErrorNoPerson : State()
    object ErrorNoResponse : State()
    object ErrorUnSuccessResponse : State()
    data class PersonLoaded(val person: PersonModel) : State()
    data class Error(val message: String) : State()
}
