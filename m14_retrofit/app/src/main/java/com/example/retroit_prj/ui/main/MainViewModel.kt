package com.example.retroit_prj.ui.main

import android.widget.ImageView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.bumptech.glide.Glide
import com.example.retroit_prj.PersonModel.PersonModel
import com.example.retroit_prj.PersonModel.PersonModelList
import com.example.retroit_prj.R
import com.example.retroit_prj.RetrofitInstance
import com.example.retroit_prj.State
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import retrofit2.Response

class MainViewModel : ViewModel() {
    private val _state = MutableStateFlow<State>(State.ReadyForSearch)
    val state = _state.asStateFlow()

    fun onGetPersonButtonClick(fragment: Fragment, photoImageView: ImageView) {
        lateinit var response: Response<PersonModelList>
        var person: PersonModel?

        viewModelScope.launch {
            _state.value = State.LoadingPerson
            try {
                response = RetrofitInstance.randomUserApi.getPersonList()
                if (response != null) {
                    if (response.isSuccessful) {
                        person = response.body()?.personList?.first()
                        if (person != null) {
                            _state.value = State.PersonLoaded(person!!)

                            if (person!!.pictureInfo.largeUrl.isNotEmpty()) {
                                _state.value = State.LoadingPicture
                                Glide.with(fragment)
                                    .load(person!!.pictureInfo.largeUrl)
                                    .into(photoImageView)
                            } else {
                                photoImageView.setImageResource(R.drawable.no_image)
                            }
                        } else
                            _state.value = State.ErrorNoPerson
                    } else
                        _state.value = State.ErrorUnSuccessResponse
                } else
                    _state.value = State.ErrorNoResponse
            } catch (e: Throwable) {
                _state.value = State.Error("$e")
            }

            _state.value = State.ReadyForSearch
        }
    }
}