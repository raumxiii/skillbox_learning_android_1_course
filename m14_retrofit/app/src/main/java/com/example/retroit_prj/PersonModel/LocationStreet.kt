package com.example.retroit_prj.PersonModel

import com.google.gson.annotations.SerializedName

data class LocationStreet(
    @SerializedName("number") val number: Int,
    @SerializedName("name") val name: String
)
