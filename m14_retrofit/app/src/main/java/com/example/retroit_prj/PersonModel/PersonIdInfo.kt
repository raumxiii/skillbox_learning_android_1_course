package com.example.retroit_prj.PersonModel

import com.google.gson.annotations.SerializedName

data class PersonIdInfo(
    @SerializedName("name") val name: String,
    @SerializedName("value") val value: String
)
