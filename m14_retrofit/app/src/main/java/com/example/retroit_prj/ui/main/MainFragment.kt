package com.example.retroit_prj.ui.main

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.TextView
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.example.retroit_prj.R
import com.example.retroit_prj.State
import com.example.retroit_prj.databinding.FragmentMainBinding
import com.google.android.material.snackbar.Snackbar
import java.text.SimpleDateFormat
import java.util.*

private const val MAIN_LOG_TAG = "MAIN_LOG"

class MainFragment : Fragment() {

    companion object {
        fun newInstance() = MainFragment()
    }

    private lateinit var binding: FragmentMainBinding
    private val viewModel: MainViewModel by viewModels()
    private val dateFormatFull = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    private val dateFormat = SimpleDateFormat("dd-MM-yyyy")
    private var birthDate: Calendar = Calendar.getInstance()
    private var birthDateString: String = ""
    private var locationFullString: String = ""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentMainBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val thisFragment = this

        binding.getPersonButton.setOnClickListener {
            viewModel.onGetPersonButtonClick(thisFragment, binding.photoMainImageView)
        }

        viewLifecycleOwner.lifecycleScope.launchWhenStarted {
            viewModel.state.collect { state ->
                when (state) {
                    State.ErrorNoPerson -> {
                        Log.e(MAIN_LOG_TAG, resources.getString(R.string.no_person_error_text))
                        binding.personSearchProgressbar.visibility = ProgressBar.INVISIBLE
                        binding.photoLoadProgressbar.visibility = ProgressBar.INVISIBLE
                        Snackbar.make(
                            view,
                            resources.getString(R.string.no_person_error_text),
                            Snackbar.LENGTH_SHORT
                        ).show()
                    }
                    State.ErrorNoResponse -> {
                        Log.e(MAIN_LOG_TAG, resources.getString(R.string.no_response_error_text))
                        binding.personSearchProgressbar.visibility = ProgressBar.INVISIBLE
                        binding.photoLoadProgressbar.visibility = ProgressBar.INVISIBLE
                        Snackbar.make(
                            view,
                            resources.getString(R.string.no_response_error_text),
                            Snackbar.LENGTH_SHORT
                        ).show()
                    }
                    State.ErrorUnSuccessResponse -> {
                        Log.e(
                            MAIN_LOG_TAG,
                            resources.getString(R.string.unsuccess_response_error_text)
                        )
                        binding.personSearchProgressbar.visibility = ProgressBar.INVISIBLE
                        binding.photoLoadProgressbar.visibility = ProgressBar.INVISIBLE
                        Snackbar.make(
                            view,
                            resources.getString(R.string.unsuccess_response_error_text),
                            Snackbar.LENGTH_SHORT
                        ).show()
                    }
                    State.ReadyForSearch -> {
                        binding.personSearchProgressbar.visibility = ProgressBar.INVISIBLE
                        binding.photoLoadProgressbar.visibility = ProgressBar.INVISIBLE
                        binding.getPersonButton.isEnabled = true
                    }
                    State.LoadingPerson -> {
                        binding.getPersonButton.isEnabled = false
                        binding.personSearchProgressbar.visibility = ProgressBar.VISIBLE
                    }
                    State.LoadingPicture -> {
                        binding.getPersonButton.isEnabled = false
                        binding.photoLoadProgressbar.visibility = ProgressBar.VISIBLE
                    }
                    is State.Error -> {
                        Log.e(MAIN_LOG_TAG, state.message)
                        binding.personSearchProgressbar.visibility = ProgressBar.INVISIBLE
                        binding.photoLoadProgressbar.visibility = ProgressBar.INVISIBLE
                        Snackbar.make(
                            view,
                            resources.getString(R.string.default_error_text),
                            Snackbar.LENGTH_SHORT
                        ).show()
                    }
                    is State.PersonLoaded -> {
                        binding.personSearchProgressbar.visibility = ProgressBar.INVISIBLE
                        binding.photoLoadProgressbar.visibility = ProgressBar.INVISIBLE
                        val person = state.person
                        locationFullString = ""

                        showLineValue(
                            binding.firstNameLine,
                            binding.firstNameTextView,
                            person.name.firstName
                        )
                        showLineValue(
                            binding.lastNameLine,
                            binding.lastNameTextView,
                            person.name.lastName
                        )
                        showLineValue(
                            binding.emailLine,
                            binding.emailTextView,
                            person.email
                        )
                        showLineValue(
                            binding.phoneLine,
                            binding.phoneTextView,
                            person.phone
                        )
                        showLineValue(
                            binding.cellLine,
                            binding.cellTextView,
                            person.cell
                        )

                        try {
                            birthDate.time =
                                dateFormatFull.parse(person.dateOfBirth.birthDateString)
                            birthDateString = dateFormat.format(birthDate.time)
                        } catch (t: Throwable) {
                            birthDateString = ""
                        }
                        showLineValue(
                            binding.birthDayLine,
                            binding.birthDayTextView,
                            birthDateString
                        )

                        if (person.location.postcode.toString().isNotEmpty())
                            locationFullString += person.location.postcode.toString()

                        if (person.location.country.trim().isNotEmpty()) {
                            if (locationFullString.isNotEmpty())
                                locationFullString += ", "
                            locationFullString += person.location.country.trim()
                        }
                        if (person.location.state.trim().isNotEmpty()) {
                            if (locationFullString.isNotEmpty())
                                locationFullString += ", "
                            locationFullString += "${resources.getString(R.string.state_prefix)}${person.location.state.trim()}"
                        }

                        if (person.location.city.trim().isNotEmpty()) {
                            if (locationFullString.isNotEmpty())
                                locationFullString += ", "
                            locationFullString += "${resources.getString(R.string.city_prefix)}${person.location.city.trim()}"
                        }

                        if (person.location.street.name.trim().isNotEmpty()) {
                            if (locationFullString.isNotEmpty())
                                locationFullString += ", "
                            locationFullString += "${resources.getString(R.string.street_name_prefix)}${person.location.street.name.trim()}"

                            if (person.location.street.number.toString().isNotEmpty())
                                locationFullString += " ${resources.getString(R.string.street_number_prefix)}${person.location.street.number.toString()}"
                        }

                        showLineValue(
                            binding.locationLine,
                            binding.locationTextView,
                            locationFullString
                        )
                    }

                }
            }
        }
    }

    private fun showLineValue(lineLayout: LinearLayout, textView: TextView, value: String) {
        if (value.trim().isNotEmpty()) {
            lineLayout.visibility = LinearLayout.VISIBLE
            textView.text = value.trim()
        } else
            lineLayout.visibility = LinearLayout.GONE
    }
}