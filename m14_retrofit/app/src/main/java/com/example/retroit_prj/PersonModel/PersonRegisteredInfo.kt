package com.example.retroit_prj.PersonModel

import com.google.gson.annotations.SerializedName

data class PersonRegisteredInfo(
    @SerializedName("date") val date: String,
    @SerializedName("age") val age: Int
)
