package com.example.retroit_prj.PersonModel

import com.google.gson.annotations.SerializedName

data class PersonModelList(
    @SerializedName("results") val personList: List<PersonModel>
)
