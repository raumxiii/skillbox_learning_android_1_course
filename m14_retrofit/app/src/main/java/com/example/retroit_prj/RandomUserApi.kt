package com.example.retroit_prj

import com.example.retroit_prj.PersonModel.PersonModelList
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Headers

private const val BASE_URL = "https://randomuser.me"

object RetrofitInstance {
    private val retrofit =
        Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    val randomUserApi: RandomUserApi = retrofit.create(RandomUserApi::class.java)
}

interface RandomUserApi {
    @Headers(
        "Accept: application/json",
        "Content-type: application/json"
    )
    @GET("api/")
    suspend fun getPersonList(): Response<PersonModelList>
}