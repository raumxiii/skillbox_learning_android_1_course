package com.example.retroit_prj.PersonModel

import com.google.gson.annotations.SerializedName

data class LocationTimezone(
    @SerializedName("offset") val offset: String,
    @SerializedName("description") val description: String
)
