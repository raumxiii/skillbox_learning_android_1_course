package com.example.retroit_prj.PersonModel

import com.google.gson.annotations.SerializedName
import java.text.SimpleDateFormat
import java.util.*

data class PersonDateOfBirth(
    @SerializedName("date") val birthDateString: String,
    @SerializedName("age") val age: Int
)