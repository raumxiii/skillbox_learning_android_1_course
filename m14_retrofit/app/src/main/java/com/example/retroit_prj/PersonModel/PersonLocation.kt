package com.example.retroit_prj.PersonModel

import com.example.retroit_prj.PersonModel.LocationCoordinates
import com.example.retroit_prj.PersonModel.LocationStreet
import com.example.retroit_prj.PersonModel.LocationTimezone
import com.google.gson.annotations.SerializedName

data class PersonLocation(
    @SerializedName("street") val street: LocationStreet,
    @SerializedName("city") val city: String,
    @SerializedName("state") val state: String,
    @SerializedName("country") val country: String,
    @SerializedName("postcode") val postcode: Int,
    @SerializedName("coordinates") val coordinates: LocationCoordinates,
    @SerializedName("timezone") val timezone: LocationTimezone
)
