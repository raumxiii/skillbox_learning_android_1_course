package com.example.retroit_prj.PersonModel

import com.google.gson.annotations.SerializedName

data class PersonPictureInfo(
    @SerializedName("large") val largeUrl: String,
    @SerializedName("medium") val mediumUrl: String,
    @SerializedName("thumbnail") val thumbnailUrl: String
)
