package com.example.retroit_prj.PersonModel

import com.google.gson.annotations.SerializedName

data class PersonName(
    @SerializedName("title") val title: String,
    @SerializedName("last") val lastName: String,
    @SerializedName("first") val firstName: String
)