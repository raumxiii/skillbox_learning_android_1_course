package com.example.retroit_prj.PersonModel

import com.google.gson.annotations.SerializedName

data class PersonModel(
    @SerializedName("gender") val gender: String,
    @SerializedName("name") val name: PersonName,
    @SerializedName("location") val location: PersonLocation,
    @SerializedName("email") val email: String,
    @SerializedName("login") val login: PersonLogin,
    @SerializedName("dob") val dateOfBirth: PersonDateOfBirth,
    @SerializedName("registered") val registeredInfo: PersonRegisteredInfo,
    @SerializedName("phone") val phone: String,
    @SerializedName("cell") val cell: String,
    @SerializedName("id") val idInfo: PersonIdInfo,
    @SerializedName("picture") val pictureInfo: PersonPictureInfo,
    @SerializedName("nat") val nat: String

)



