package com.example.room_prj.db


import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = [Word::class], version = 1)
abstract class AppDB : RoomDatabase() {
    abstract fun wordDao(): WordDao
}