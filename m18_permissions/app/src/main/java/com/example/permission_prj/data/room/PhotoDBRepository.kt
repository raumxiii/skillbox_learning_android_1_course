package com.example.permission_prj.data.room

import com.example.permission_prj.data.PhotoDBDto
import com.example.permission_prj.entity.Photo
import javax.inject.Inject

class PhotoDBRepository @Inject constructor(private val photoDao: PhotoDao) {
    suspend fun getPhotoList(): List<Photo> {
        val photoList =  photoDao.getPhotoList()
        println("photoList count = ${photoList.size} ")
        return photoList
    }

    suspend fun savePhoto(photo: PhotoDBDto) {
        photoDao.insert(photo)
    }
}