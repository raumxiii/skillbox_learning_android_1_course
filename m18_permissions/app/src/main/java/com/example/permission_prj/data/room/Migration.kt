package com.example.permission_prj.data.room

import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase

val MIGRATION_1_2: Migration = object : Migration(1, 2) {
    override fun migrate(database: SupportSQLiteDatabase) {
        database.execSQL("CREATE TABLE photo_table_tmp( id INTEGER PRIMARY KEY AUTOINCREMENT, date_create TEXT NOT NULL, src_uri TEXT  NOT NULL)")

        database.execSQL("INSERT INTO photo_table_tmp( id, date_create, src_uri ) SELECT id, date_create, src_uri from photo_table")

        database.execSQL("DROP TABLE photo_table")

        database.execSQL("ALTER TABLE photo_table_tmp RENAME TO photo_table")
    }

}