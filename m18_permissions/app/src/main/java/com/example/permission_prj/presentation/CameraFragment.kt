package com.example.permission_prj.presentation

import android.Manifest
import android.content.ContentValues
import android.content.Context
import android.content.pm.PackageManager
import android.icu.text.SimpleDateFormat
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.camera.core.CameraSelector
import androidx.camera.core.ImageCapture
import androidx.camera.core.ImageCaptureException
import androidx.camera.core.Preview
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.core.content.ContextCompat
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.example.permission_prj.R
import com.example.permission_prj.data.CallerList
import com.example.permission_prj.data.PhotoDBDto
import com.example.permission_prj.databinding.FragmentCameraBinding
import dagger.hilt.android.AndroidEntryPoint
import java.util.*
import java.util.concurrent.Executor

private const val FILENAME_FORMAT = "yyyy-MM-dd-HH-mm-ss"

@AndroidEntryPoint
class CameraFragment : Fragment() {

    private lateinit var executor: Executor
    private lateinit var myContext: Context

    private val name =
        SimpleDateFormat(FILENAME_FORMAT, Locale.US).format(System.currentTimeMillis())

    private var _binding: FragmentCameraBinding? = null
    private val binding get() = _binding!!
    private val viewModel: CameraViewModel by viewModels()
    private var lastPhoto: PhotoDBDto? = null
    private var imageCapture: ImageCapture? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        _binding = FragmentCameraBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        myContext = this.context!!
        executor = ContextCompat.getMainExecutor(myContext)

        checkPermissions()
        refreshPhoto()

        binding.returnFromCameraButton.setOnClickListener {
            findNavController().navigate(R.id.action_cameraFragment_to_mainFragment)
        }

        binding.getPhotoButton.setOnClickListener {
            getPhoto()
        }

        binding.smallPhotoPreview.setOnClickListener {
            lastPhoto?.let {
                val bundle = Bundle().apply {

                    val photoToShow = lastPhoto
                    putString("srcUri", photoToShow!!.srcUri)
                    putString("dataCreateStr", photoToShow.dateCreateStr)

                    putString("caller", CallerList.CAMERA.toString())
                }
                findNavController().navigate(
                    R.id.action_cameraFragment_to_photoViewFragment,
                    bundle
                )
            }
        }
    }

    private fun refreshPhoto() {
        lastPhoto = viewModel.lastPhoto

        lastPhoto?.let {
            Glide
                .with(binding.smallPhotoPreview.context)
                .load(lastPhoto!!.srcUri)
                .into(binding.smallPhotoPreview)
        }
    }

    private fun getPhoto() {
        val imageCapture = imageCapture ?: return

        val contentValues = ContentValues().apply {
            put(MediaStore.MediaColumns.DISPLAY_NAME, name)
            put(MediaStore.MediaColumns.MIME_TYPE, "image/jpeg")
        }

        val outputFileOptions = ImageCapture.OutputFileOptions.Builder(
            myContext.contentResolver,
            MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
            contentValues
        ).build()

        imageCapture.takePicture(
            outputFileOptions,
            executor,
            object : ImageCapture.OnImageSavedCallback {
                override fun onImageSaved(outputFileResults: ImageCapture.OutputFileResults) {
                    Toast.makeText(
                        myContext,
                        "Photo Saved On ${outputFileResults.savedUri}",
                        Toast.LENGTH_SHORT
                    ).show()

                    val newPhoto = PhotoDBDto(
                        dateCreateStr = SimpleDateFormat(
                            FILENAME_FORMAT,
                            Locale.US
                        ).format(System.currentTimeMillis()),
                        srcUri = outputFileResults.savedUri.toString()
                    )
                    newPhoto.let {
                        viewModel.savePhoto(newPhoto)
                        refreshPhoto()
                    }

                }

                override fun onError(exception: ImageCaptureException) {
                    Toast.makeText(
                        myContext,
                        "Photo failed: ${exception.message}",
                        Toast.LENGTH_SHORT
                    ).show()
                    exception.printStackTrace()
                }
            }

        )
    }

    private fun startCamera() {
        val cameraProviderFuture = ProcessCameraProvider.getInstance(myContext)
        cameraProviderFuture.addListener({
            val cameraProvider = cameraProviderFuture.get()
            val preview = Preview.Builder().build()
            preview.setSurfaceProvider(binding.cameraPreview.surfaceProvider)
            imageCapture = ImageCapture.Builder().build()

            cameraProvider.unbindAll()
            cameraProvider.bindToLifecycle(
                this,
                CameraSelector.DEFAULT_BACK_CAMERA,
                preview,
                imageCapture
            )
        }, executor)
    }

    private val launcher =
        registerForActivityResult(ActivityResultContracts.RequestMultiplePermissions()) { map ->
            if (map.values.all { it }) {
                startCamera()
            } else {
                Toast.makeText(myContext, "Permission is not Granted", Toast.LENGTH_SHORT).show()
            }
        }

    private fun checkPermissions() {
        val isAllGranted = REQUEST_PERMISSION_LIST.all { permission ->
            ContextCompat.checkSelfPermission(
                myContext,
                permission
            ) == PackageManager.PERMISSION_GRANTED
        }

        if (isAllGranted) {
            startCamera()
            Toast.makeText(myContext, "Permission is Granted", Toast.LENGTH_SHORT).show()
        } else
            launcher.launch(REQUEST_PERMISSION_LIST)
    }

    companion object {
        private val REQUEST_PERMISSION_LIST: Array<String> = buildList {
            add(Manifest.permission.CAMERA)
            if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.P) {
                add(Manifest.permission.WRITE_EXTERNAL_STORAGE)
            }
        }.toTypedArray()
    }
}