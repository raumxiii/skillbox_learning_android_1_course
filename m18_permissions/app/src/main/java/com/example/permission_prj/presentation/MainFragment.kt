package com.example.permission_prj.presentation

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.example.permission_prj.R
import com.example.permission_prj.data.CallerList
import com.example.permission_prj.databinding.FragmentMainBinding
import com.example.permission_prj.entity.Photo
import com.example.permission_prj.presentation.PhotoRecyclerView.PhotoRecyclerViewAdapter

import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

@AndroidEntryPoint
class MainFragment : Fragment() {

    private var _binding: FragmentMainBinding? = null
    private val binding get() = _binding!!
    private val viewModel: MainViewModel by viewModels()
    private val photoRecyclerViewAdapter = PhotoRecyclerViewAdapter { photo -> onItemClick(photo) }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentMainBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.toCameraViewImageView.setOnClickListener {
            findNavController().navigate(R.id.action_mainFragment_to_cameraFragment)
        }

        binding.photoGridRecycler.adapter = photoRecyclerViewAdapter
        viewModel.getPhotoList()
        viewModel.photoList.onEach { it?.let { photoRecyclerViewAdapter.setData(it) } }
            .launchIn(viewLifecycleOwner.lifecycleScope)
    }

    private fun onItemClick(item: Photo) {
        val bundle = Bundle().apply {
            putString("srcUri", item.srcUri)
            putString("dataCreateStr", item.dateCreateStr)
            putString("caller", CallerList.MAIN.toString())
        }
        findNavController().navigate(R.id.action_mainFragment_to_photoViewFragment, bundle)
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}