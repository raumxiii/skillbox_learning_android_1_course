package com.example.permission_prj.entity

interface Photo {
    val id: Int?
    val dateCreateStr: String
    val srcUri: String
}