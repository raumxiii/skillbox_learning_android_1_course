package com.example.permission_prj.presentation.PhotoRecyclerView

import androidx.recyclerview.widget.RecyclerView
import com.example.permission_prj.databinding.PhotoGridItemBinding

class PhotoViewHolder(val binding: PhotoGridItemBinding) :
    RecyclerView.ViewHolder(binding.root)