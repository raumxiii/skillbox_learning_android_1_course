package com.example.permission_prj.data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.permission_prj.entity.Photo

@Entity(tableName = "photo_table")
data class PhotoDBDto(
    @PrimaryKey
    @ColumnInfo(name = "id")
    override val id: Int? = null,
    @ColumnInfo(name = "date_create")
    override val dateCreateStr: String,
    @ColumnInfo(name = "src_uri")
    override val srcUri: String
) : Photo