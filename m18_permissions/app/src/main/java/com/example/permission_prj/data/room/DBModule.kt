package com.example.permission_prj.data.room

import android.content.Context
import androidx.room.Room
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class DBModule {
    @Provides
    fun providePhotoDap(appDB: AppDB): PhotoDao {
        return appDB.photoDao()
    }

    @Provides
    @Singleton
    fun provideAppDB(@ApplicationContext appContext: Context):
            AppDB {
        return Room.databaseBuilder(
            appContext,
            AppDB::class.java,
            "db"
        )
            .addMigrations(MIGRATION_1_2)
            .build()
    }
}