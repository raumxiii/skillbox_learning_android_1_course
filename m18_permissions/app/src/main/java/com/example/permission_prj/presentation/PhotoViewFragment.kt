package com.example.permission_prj.presentation

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.example.permission_prj.R
import com.example.permission_prj.data.CallerList
import com.example.permission_prj.databinding.FragmentPhotoViewBinding

private const val ARG_PARAM1 = "srcUri"
private const val ARG_PARAM2 = "dataCreateStr"
private const val ARG_PARAM3 = "caller"

class PhotoViewFragment : Fragment() {
    private var srcUri: String? = null
    private var dataCreateStr: String? = null
    private var caller: CallerList? = null

    private var _binding: FragmentPhotoViewBinding? = null
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            srcUri = it.getString(ARG_PARAM1)
            dataCreateStr = it.getString(ARG_PARAM2)
            caller = CallerList.valueOf(it.getString(ARG_PARAM3)!!)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        _binding = FragmentPhotoViewBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        srcUri?.let {
            Glide
                .with(binding.photoView.context)
                .load(srcUri)
                .into(binding.photoView)
            binding.createDateTextView.text = dataCreateStr
        }

        binding.photoView.setOnClickListener {
            findNavController().navigate(
                when (caller) {
                    CallerList.CAMERA -> R.id.action_photoViewFragment_to_cameraFragment
                    else -> R.id.action_photoViewFragment_to_mainFragment
                }
            )
        }
    }

    companion object {
        @JvmStatic
        fun newInstance(param1: String, param2: String, param3: String) =
            PhotoViewFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                    putString(ARG_PARAM3, param3)
                }
            }
    }
}
