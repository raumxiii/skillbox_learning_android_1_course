package com.example.permission_prj.presentation.PhotoRecyclerView

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.permission_prj.databinding.PhotoGridItemBinding
import com.example.permission_prj.entity.Photo

class PhotoRecyclerViewAdapter(private val onClickAction: (Photo) -> Unit) :
    RecyclerView.Adapter<PhotoViewHolder>() {
    private var photoList: List<Photo> = emptyList()

    fun setData(photoList: List<Photo>) {
        this.photoList = photoList
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PhotoViewHolder {
        return PhotoViewHolder(
            PhotoGridItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: PhotoViewHolder, position: Int) {
        val item = photoList.getOrNull(position)
        with(holder.binding) {
            createDateTextView.text = item?.dateCreateStr
            item?.let {
                Glide
                    .with(photoImage.context)
                    .load(it.srcUri)
                    .into(photoImage)
            }
        }

        holder.binding.root.setOnClickListener {
            item?.let { onClickAction(item) }
        }
    }

    override fun getItemCount(): Int {
        return photoList.size
    }
}