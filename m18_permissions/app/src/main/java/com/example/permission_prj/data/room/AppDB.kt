package com.example.permission_prj.data.room

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.permission_prj.data.PhotoDBDto

@Database(entities = [PhotoDBDto::class], version = 2)
abstract class AppDB : RoomDatabase() {
    abstract fun photoDao(): PhotoDao
}