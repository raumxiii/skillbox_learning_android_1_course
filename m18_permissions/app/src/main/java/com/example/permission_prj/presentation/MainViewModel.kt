package com.example.permission_prj.presentation

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.permission_prj.data.room.PhotoDBRepository
import com.example.permission_prj.entity.Photo
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(private val repository: PhotoDBRepository) : ViewModel() {
    private val _photoList = MutableStateFlow<List<Photo>?>(emptyList())
    val photoList = _photoList.asStateFlow()

    fun getPhotoList() {
        viewModelScope.launch(Dispatchers.IO) {
            kotlin.runCatching {
                repository.getPhotoList()
            }.fold(
                onSuccess = { _photoList.value = it },
                onFailure = { Log.d("MainViewModel", it.message ?: "") }
            )
        }
    }
}