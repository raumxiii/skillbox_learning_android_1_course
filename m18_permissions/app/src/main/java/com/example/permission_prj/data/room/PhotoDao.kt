package com.example.permission_prj.data.room

import androidx.room.*
import com.example.permission_prj.data.PhotoDBDto

@Dao
interface PhotoDao {
    @Query("SELECT * FROM photo_table Order by id")
    suspend fun getPhotoList(): List<PhotoDBDto>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(photo: PhotoDBDto)

    @Query("DELETE FROM photo_table where id = :id")
    suspend fun delete(id: Int)
}