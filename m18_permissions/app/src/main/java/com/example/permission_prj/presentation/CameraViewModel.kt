package com.example.permission_prj.presentation

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.permission_prj.data.PhotoDBDto
import com.example.permission_prj.data.room.PhotoDBRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class CameraViewModel @Inject constructor(private val repository: PhotoDBRepository) : ViewModel() {
    private var _lastPhoto: PhotoDBDto? = null
    val lastPhoto get() = _lastPhoto

    fun savePhoto(newPhoto: PhotoDBDto) {
        _lastPhoto = newPhoto
        viewModelScope.launch(Dispatchers.IO) {
            repository.savePhoto(newPhoto)
        }
    }
}