package com.example.location_prj.data

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class FeatureCollection(
    @Json(name = "type") val type: String,
    @Json(name = "features") val features: List<InterestPlaceDto>
)
