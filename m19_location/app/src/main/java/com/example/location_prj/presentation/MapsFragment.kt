package com.example.location_prj.presentation

import android.Manifest
import android.annotation.SuppressLint
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import androidx.fragment.app.Fragment

import android.os.Bundle
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.example.location_prj.R
import com.example.location_prj.databinding.FragmentMapsBinding
import com.google.android.gms.location.*
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.messaging.FirebaseMessaging
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

@AndroidEntryPoint
class MapsFragment : Fragment() {
    companion object {
        fun newInstance() = MapsFragment()

        private const val LOG_TAG = "Maps_Fragment"

        private val REQUEST_PERMISSION_LIST: Array<String> = buildList {
            add(Manifest.permission.ACCESS_FINE_LOCATION)
            add(Manifest.permission.ACCESS_COARSE_LOCATION)
        }.toTypedArray()

        private const val NOTIFICATION_ID = 1000
    }

    private lateinit var myContext: Context
    private lateinit var fusedClient: FusedLocationProviderClient
    private var interestPointRadius: Float = 1000f
    private var map: GoogleMap? = null
    private var locationListener: LocationSource.OnLocationChangedListener? = null
    private var myLocation: LocationResult? = null
    private var _binding: FragmentMapsBinding? = null
    private val binding get() = _binding!!
    private val viewModel: MapsViewModel by viewModels()

    private var needPointList = true

    private val launcher =
        registerForActivityResult(ActivityResultContracts.RequestMultiplePermissions()) { map ->
            if (map.values.all { it }) {
                startLocation()
            } else {
                Toast.makeText(myContext, "Permission is not Granted", Toast.LENGTH_SHORT).show()
            }
        }

    private val locationCallback = object : LocationCallback() {
        override fun onLocationResult(result: LocationResult) {
            result.lastLocation?.let { location ->
                locationListener?.onLocationChanged(location)
                myLocation = result
                map?.let {
                    val myLocationAxis =
                        LatLng(
                            myLocation?.lastLocation?.latitude!!,
                            myLocation?.lastLocation?.longitude!!
                        )
                    if (viewModel.followMyLocation) {
                        map?.animateCamera(CameraUpdateFactory.newLatLngZoom(myLocationAxis, 15f))
                        createNotification("Info", "You`re founded")
                        viewModel.followMyLocation = false
                    }
                    viewModel.getInterestPlaceList(
                        (myLocationAxis.longitude.toFloat() - interestPointRadius / 1000),
                        (myLocationAxis.latitude.toFloat() - interestPointRadius / 1000),
                        (myLocationAxis.longitude.toFloat() + interestPointRadius / 1000),
                        (myLocationAxis.latitude.toFloat() + interestPointRadius / 1000),
                        resources.getString(R.string.open_trip_map_api_key)
                    )
                }
            }

            if (needPointList && viewModel.wasLoaded.value) {
                map?.let {
                    viewModel.interestPlaceList.onEach {
                        it?.let {
                            it.onEach { interestPlace ->
                                var snippetValue = ""
                                interestPlace.property.wikidata?.let {
                                    snippetValue = "wikidata: ${interestPlace.property.wikidata}\n"
                                }
                                snippetValue += "rate: ${interestPlace.property.rate}"


                                map!!.addMarker(
                                    MarkerOptions()
                                        .position(
                                            LatLng(
                                                interestPlace.geometry.coordinates[1].toDouble(),
                                                interestPlace.geometry.coordinates[0].toDouble()
                                            )
                                        )
                                        .title(interestPlace.property.name)
                                        .snippet(snippetValue)
                                )
                            }
                        }

                    }.launchIn(viewLifecycleOwner.lifecycleScope)
                    needPointList = false
                }
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentMapsBinding.inflate(inflater, container, false)
        myContext = this.requireContext()
        fusedClient = LocationServices.getFusedLocationProviderClient(myContext)

        checkPermissions()

        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment?.getMapAsync { googleMap ->
            with(googleMap.uiSettings) {
                isZoomControlsEnabled = true
                isMyLocationButtonEnabled = true
            }
            googleMap.setLocationSource(object : LocationSource {
                override fun activate(p0: LocationSource.OnLocationChangedListener) {
                    locationListener = p0
                }

                override fun deactivate() {
                    locationListener = null
                }
            })
            @SuppressLint("MissingPermission")
            googleMap.isMyLocationEnabled = true
            map = googleMap
        }

        return binding.root
    }

    override fun onStart() {
        super.onStart()
        interestPointRadius = resources.getInteger(R.integer.interest_point_radius).toFloat()
        checkPermissions()

        FirebaseMessaging.getInstance().token.addOnCompleteListener(OnCompleteListener { task ->
            if (!task.isSuccessful) {
                Log.w(LOG_TAG, "Fetching FCM registration token failed", task.exception)
                return@OnCompleteListener
            }

            // Get new FCM registration token
            val token = task.result

            // Log and toast
            Log.d(LOG_TAG, "token = $token")

        })
    }

    override fun onStop() {
        super.onStop()
        fusedClient.removeLocationUpdates(locationCallback)
    }

    @SuppressLint("MissingPermission")
    private fun startLocation() {
        val locationRequest = LocationRequest.create()
            .setInterval(1000)
            .setPriority(Priority.PRIORITY_HIGH_ACCURACY)

        fusedClient.requestLocationUpdates(
            locationRequest,
            locationCallback,
            Looper.getMainLooper()
        )
    }

    private fun checkPermissions() {
        val isAllGranted = REQUEST_PERMISSION_LIST.all { permission ->
            ContextCompat.checkSelfPermission(
                myContext,
                permission
            ) == PackageManager.PERMISSION_GRANTED
        }

        if (isAllGranted)
            startLocation()
        else
            launcher.launch(REQUEST_PERMISSION_LIST)
    }

    fun createNotification(title: String, text: String) {
        val intent = Intent(requireContext(), MainActivity::class.java)

        val pendingIntent =
            PendingIntent.getActivity(
                requireContext(),
                0,
                intent,
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S)
                    PendingIntent.FLAG_IMMUTABLE
                else
                    PendingIntent.FLAG_UPDATE_CURRENT
            )
        val notification =
            NotificationCompat.Builder(requireContext(), MainActivity.MAIN_NOTIFICATION_CHANNEL_ID)
                .setSmallIcon(R.drawable.notification_icon)
                .setContentTitle(title)
                .setContentText(text)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setContentIntent(pendingIntent)
                .setAutoCancel(true)
                .build()

        NotificationManagerCompat.from(requireContext()).notify(NOTIFICATION_ID, notification)

    }

}