package com.example.location_prj.data

import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query


private const val BASE_URL = "https://api.opentripmap.com/0.1/ru/"

interface OpenTripMapDataSource {
    @GET("places/bbox")
    suspend fun getPlaceList(
        @Query("lon_min") lonMin: Float,
        @Query("lat_min") latMin: Float,
        @Query("lon_max") lonMax: Float,
        @Query("lat_max") latMax: Float,
        @Query("apikey") apikey: String,
        @Query("kinds") kinds: String = "interesting_places",
        @Query("kinds") format: String = "geojson"
    ): Response<FeatureCollection>
}

object RetrofitInstance {
    private val retrofit =
        Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(MoshiConverterFactory.create())
            .build()
    val openTripMapDataSource = retrofit.create(OpenTripMapDataSource::class.java)
}