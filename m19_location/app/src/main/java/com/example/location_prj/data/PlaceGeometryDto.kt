package com.example.location_prj.data

import com.example.location_prj.entity.PlaceGeometry
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class PlaceGeometryDto(
    @Json(name = "type") override val type: String,
    @Json(name = "coordinates") override val coordinates: Array<Float>
) : PlaceGeometry
