package com.example.location_prj.presentation

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.location_prj.data.GetOpenTripInterestPlaceListUseCase
import com.example.location_prj.entity.InterestPlace
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MapsViewModel @Inject constructor(
    private val openTripInterestPlaceListUseCase: GetOpenTripInterestPlaceListUseCase
) : ViewModel() {
    var followMyLocation = true
    private val _wasLoaded = MutableStateFlow(false)
    val wasLoaded = _wasLoaded.asStateFlow()
    private var _interestPlaceList = MutableStateFlow<List<InterestPlace>?>(emptyList())
    val interestPlaceList = _interestPlaceList

    fun getInterestPlaceList(
        lonMin: Float,
        latMin: Float,
        lonMax: Float,
        latMax: Float,
        apikey: String
    ) {
        if (!_wasLoaded.value) {
            viewModelScope.launch(Dispatchers.IO) {
                kotlin.runCatching {
                    openTripInterestPlaceListUseCase.execute(
                        lonMin,
                        latMin,
                        lonMax,
                        latMax,
                        apikey
                    )
                }.fold(
                    onSuccess = {
                        it?.let {
                            _interestPlaceList.value = it
                            _wasLoaded.value = true
                        }
                    },
                    onFailure = { Log.d("MapsViewModel", it.message ?: "") }
                )
            }
        }
    }
}