package com.example.location_prj.data

import com.example.location_prj.entity.PlaceProperty
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class PlacePropertyDto(
    @Json(name = "xid") override val xid: String,
    @Json(name = "name") override val name: String,
    @Json(name = "rate") override val rate: Int,
    @Json(name = "osm") override val osm: String?,
    @Json(name = "wikidata") override val wikidata: String?,
    @Json(name = "kinds") override val kinds: String?
) : PlaceProperty
