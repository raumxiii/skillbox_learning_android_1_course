package com.example.location_prj.data

import android.util.Log
import retrofit2.Response
import javax.inject.Inject

private const val LOG_TAG = "OpenTripMapRepository_Log"

class OpenTripMapRepository @Inject constructor() {
    suspend fun getPlaceList(
        lonMin: Float,
        latMin: Float,
        lonMax: Float,
        latMax: Float,
        apikey: String
    )
            : List<InterestPlaceDto>? {
        var interestPlaceList: List<InterestPlaceDto>? = null
        try {
            val response: Response<FeatureCollection> =
                RetrofitInstance.openTripMapDataSource.getPlaceList(
                    lonMin,
                    latMin,
                    lonMax,
                    latMax,
                    apikey
                )
            if (response != null && response.isSuccessful && response.body() != null) {
                interestPlaceList = response.body()?.features
            }

        } catch (e: Throwable) {
            Log.e(
                LOG_TAG,
                "${e.toString()} \r\n !!!!!cause: ${e.cause.toString()} \r\n suppressed by: ${e.suppressed.toString()}"
            )

        } finally {
            return interestPlaceList
        }
    }

}