package com.example.location_prj.entity

interface InterestPlace {
    val type: String

    val id: String
    val geometry: PlaceGeometry
    val property: PlaceProperty
}