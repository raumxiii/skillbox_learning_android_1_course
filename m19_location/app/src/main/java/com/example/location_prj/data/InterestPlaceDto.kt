package com.example.location_prj.data

import com.example.location_prj.entity.InterestPlace
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class InterestPlaceDto(
    @Json(name = "type") override val type: String,
    @Json(name = "id") override val id: String,
    @Json(name = "geometry") override val geometry: PlaceGeometryDto,
    @Json(name = "properties") override val property: PlacePropertyDto
) : InterestPlace
