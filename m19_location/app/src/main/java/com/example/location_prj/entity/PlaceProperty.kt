package com.example.location_prj.entity

interface PlaceProperty {
    val xid: String
    val name: String
    val rate: Int
    val osm: String?
    val wikidata: String?
    val kinds: String?
}