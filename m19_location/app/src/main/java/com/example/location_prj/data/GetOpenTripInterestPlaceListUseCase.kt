package com.example.location_prj.data

import com.example.location_prj.entity.InterestPlace
import javax.inject.Inject

class GetOpenTripInterestPlaceListUseCase @Inject
constructor(private val repository: OpenTripMapRepository) {
    suspend fun execute(
        lonMin: Float,
        latMin: Float,
        lonMax: Float,
        latMax: Float,
        apikey: String
    ): List<InterestPlace>? {
        return repository.getPlaceList(
            lonMin,
            latMin,
            lonMax,
            latMax,
            apikey
        )
    }
}