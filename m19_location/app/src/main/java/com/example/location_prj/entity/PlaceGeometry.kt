package com.example.location_prj.entity

interface PlaceGeometry {
    val type: String
    val coordinates: Array<Float>
}