package com.example.prj

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.SeekBar
import android.widget.Toast
import com.example.prj.databinding.ActivityMainBinding
import kotlinx.coroutines.*

private const val KEY_IN_PROGRESS = "IN_PROGRESS"
private const val KEY_PERIOD_VALUE = "PERIOD_VALUE"
private const val KEY_REMAIN_PERIOD_VALUE = "REMAIN_PERIOD_VALUE"

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private var mainJob: Job = Job()
    private var periodValue = 0
    private var remainPeriodValue = 0
    private var isInProgress = false

    private var mainCoroutineScope = CoroutineScope(Dispatchers.Main)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Чтение сохраненных компонентов состояния
        savedInstanceState?.let { bundle: Bundle ->
            isInProgress = bundle.getBoolean(KEY_IN_PROGRESS, false)
            periodValue = bundle.getInt(KEY_PERIOD_VALUE, 0)
            remainPeriodValue = bundle.getInt(KEY_REMAIN_PERIOD_VALUE, 0)
        }

        // Биндинг элементов
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // Настройка слушателей
        binding.timePickerSeekBar.setOnSeekBarChangeListener(object :
            SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(SeekBar: SeekBar, value: Int, fromUser: Boolean) {
                remainPeriodValue = value
                refreshValues()
            }

            override fun onStartTrackingTouch(p0: SeekBar) {}
            override fun onStopTrackingTouch(p0: SeekBar) {}
        })

        binding.timePickerSlider.addOnChangeListener { _, value, _ ->
            remainPeriodValue = value.toInt()
            refreshValues()
        }

        binding.button.setOnClickListener {
            if (!isInProgress) {
                startTimerProcess()
            } else stopTimerProcess()
        }

        // Отключение джоба по-умолчанию
        if (mainJob.isActive)
            mainJob.cancel()

        // Восстановление джоба  при продолжении прогресса
        if (isInProgress && remainPeriodValue > 0) {
            refreshValues()
            runJob()
        }

    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putBoolean(KEY_IN_PROGRESS, isInProgress)
        outState.putInt(KEY_PERIOD_VALUE, periodValue)
        outState.putInt(KEY_REMAIN_PERIOD_VALUE, remainPeriodValue)

        super.onSaveInstanceState(outState)
    }

    override fun onDestroy() {
        super.onDestroy()

        // Удаление текущего джоба для предотвращения утечек памяти
        if (mainJob.isActive)
            mainJob.cancel()
    }

    private fun refreshValues() {
        // Обновление доступности элементов в зависимости от состояния обработки
        if (isInProgress) {
            binding.timePickerSlider.isEnabled = false
            binding.timePickerSeekBar.isEnabled = false
            binding.button.text = resources.getString(R.string.button_in_progress_text)
        } else {
            binding.timePickerSlider.isEnabled = true
            binding.timePickerSeekBar.isEnabled = true
            binding.button.text = resources.getString(R.string.button_text)
        }

        // Обновление прогрессбаров
        binding.timePickerSeekBar.progress = remainPeriodValue
        binding.timePickerSlider.value = remainPeriodValue.toFloat()
        binding.timeRemainTextView.text = remainPeriodValue.toString()
        if (isInProgress && periodValue > 0) {
            binding.progressBarMain.progress =
                ((1.0f - remainPeriodValue.toFloat() / periodValue.toFloat()) * 100).toInt()
        } else
            binding.progressBarMain.progress = 0
    }

    private fun runJob() {
        mainJob = mainCoroutineScope.launch {
            while (remainPeriodValue > 0) {
                delay(1000)
                remainPeriodValue--
                refreshValues()
            }
            stopTimerProcess()
        }
    }

    private fun startTimerProcess() {
        if (isInProgress)
            return

        periodValue = remainPeriodValue
        isInProgress = true

        // Обновление интерфейса (блокировка элементов)
        refreshValues()

        //Запуск самого джоба
        runJob()

        Toast.makeText(
            this,
            resources.getString(R.string.progress_start_text).format(periodValue.toString()),
            Toast.LENGTH_SHORT
        ).show()
    }

    private fun stopTimerProcess() {
        if (mainJob.isActive) {
            mainJob.cancel()
        }
        if (remainPeriodValue == 0)
            Toast.makeText(
                this,
                resources.getString(R.string.progress_end_text),
                Toast.LENGTH_SHORT
            ).show()
        else
            Toast.makeText(
                this,
                resources.getString(R.string.progress_stop_text)
                    .format(remainPeriodValue.toString()),
                Toast.LENGTH_SHORT
            ).show()

        isInProgress = false
        periodValue = 0

        // Обновление интерфейса (разблокировка элементов)
        refreshValues()
    }
}