package com.example.recycler_view_prj.data

import com.example.recycler_view_prj.entity.Camera
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class CameraDto(
    @Json(name = "id") override val id: String,
    @Json(name = "name") override val name: String,
    @Json(name = "rover_id") override val roverId: Int,
    @Json(name = "full_name") override val fullName: String
) : Camera