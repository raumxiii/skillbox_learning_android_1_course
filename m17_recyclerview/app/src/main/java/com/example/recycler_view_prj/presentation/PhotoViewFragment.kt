package com.example.recycler_view_prj.presentation

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.example.recycler_view_prj.R
import com.example.recycler_view_prj.databinding.FragmentPhotoViewBinding

private const val ARG_PARAM1 = "srcString"

class PhotoViewFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var srcString: String? = null

    private var _binding: FragmentPhotoViewBinding? = null
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            srcString = it.getString(ARG_PARAM1)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentPhotoViewBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val photoImageView = binding.photoImageView
        srcString?.let {
            Glide
                .with(photoImageView.context)
                .load(srcString)
                .into(photoImageView)
        }

        binding.photoImageView.setOnClickListener() {
            findNavController().navigate(R.id.action_photoViewFragment_to_mainFragment)
        }
    }

    companion object {
        @JvmStatic
        fun newInstance(param1: String) =
            PhotoViewFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                }
            }
    }
}