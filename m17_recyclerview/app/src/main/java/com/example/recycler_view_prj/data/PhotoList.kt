package com.example.recycler_view_prj.data

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class PhotoList(
    @Json(name = "photos") val photoList: List<PhotoDto>
)
