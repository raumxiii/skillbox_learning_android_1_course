package com.example.recycler_view_prj.data

import com.example.recycler_view_prj.entity.Rover
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class RoverDto(
    @Json(name = "id") override val id: Int,
    @Json(name = "name") override val name: String,
    @Json(name = "landing_date") override val landingDateStr: String,
    @Json(name = "launch_date") override val launchDateStr: String,
    @Json(name = "status") override val status: String
) : Rover