package com.example.recycler_view_prj.entity

interface Photo {
    val id: Int
    val sol: Int
    val camera: Camera
    val imgSrc: String
    val earthDateStr: String
    val rover: Rover
}