package com.example.recycler_view_prj.data

import android.util.Log
import com.example.recycler_view_prj.entity.Photo
import com.example.recycler_view_prj.presentation.CalendarConverter
import retrofit2.Response
import java.util.*
import javax.inject.Inject

private const val MAIN_LOG_TAG = "REP_LOG"

class NasaMarsPhotoRepository @Inject constructor() {
    suspend fun getPhotoList(
        dateView: Calendar,
        apiKey: String,
        page: Int?
    ): List<PhotoDto>? {


        var dateViewStr = CalendarConverter.getString(dateView)

        println("NasaMarsPhotoRepository! getPhotoList page = $page apiKey = $apiKey dateView = $dateViewStr")

        lateinit var response: Response<PhotoList>
        var photoList: List<PhotoDto>? = null

        try {
            response =
                RetrofitInstance.nasaMarsPhotoDataSource.getPhotoList(dateViewStr, apiKey, page)


            println("NasaMarsPhotoRepository! response = $response")

            if (response != null && response.isSuccessful && response.body() != null) {
                photoList = response?.body()?.photoList
            }
        } catch (e: Throwable) {
            Log.e(
                MAIN_LOG_TAG,
                "${e.toString()} \r\n !!!!!cause: ${e.cause.toString()} \r\n suppressed by: ${e.suppressed.toString()}"
            )
        } finally {

            println("NasaMarsPhotoRepository! photoList  = ${photoList.toString()}")

            return photoList
        }
    }
}