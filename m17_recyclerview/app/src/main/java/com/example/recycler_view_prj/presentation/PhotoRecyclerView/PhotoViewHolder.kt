package com.example.recycler_view_prj.presentation.PhotoRecyclerView

import androidx.recyclerview.widget.RecyclerView
import com.example.recycler_view_prj.databinding.PhotoItemBinding

class PhotoViewHolder(val binding: PhotoItemBinding) : RecyclerView.ViewHolder(binding.root)