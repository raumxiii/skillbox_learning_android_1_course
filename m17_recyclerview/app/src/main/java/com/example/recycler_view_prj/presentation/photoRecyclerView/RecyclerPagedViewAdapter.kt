package com.example.recycler_view_prj.presentation.PhotoRecyclerView

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import com.bumptech.glide.Glide
import com.example.recycler_view_prj.data.PhotoDto
import com.example.recycler_view_prj.databinding.PhotoItemBinding

class RecyclerPagedViewAdapter(private val onClickAction: (PhotoDto) -> Unit) :
    PagingDataAdapter<PhotoDto, PhotoViewHolder>(DiffUtilCallback()) {
    class DiffUtilCallback : DiffUtil.ItemCallback<PhotoDto>() {
        override fun areItemsTheSame(oldItem: PhotoDto, newItem: PhotoDto): Boolean =
            oldItem.id == newItem.id

        override fun areContentsTheSame(oldItem: PhotoDto, newItem: PhotoDto): Boolean =
            oldItem == newItem
    }

    override fun onBindViewHolder(holder: PhotoViewHolder, position: Int) {
        val item = getItem(position)
        with(holder.binding) {
            cameraNameText.text = item?.camera?.name
            roverNameText.text = item?.rover?.name
            dateText.text = item?.earthDateStr
            solText.text = item?.sol.toString()
            item?.let {
                Glide
                    .with(photoImageView.context)
                    .load(it.imgSrc)
                    .into(photoImageView)
            }
        }
        holder.binding.root.setOnClickListener {
            item?.let {
                onClickAction(item)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PhotoViewHolder {
        return PhotoViewHolder(
            PhotoItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }
}