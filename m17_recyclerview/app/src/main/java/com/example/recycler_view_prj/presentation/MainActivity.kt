package com.example.recycler_view_prj.presentation

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.recycler_view_prj.R
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}