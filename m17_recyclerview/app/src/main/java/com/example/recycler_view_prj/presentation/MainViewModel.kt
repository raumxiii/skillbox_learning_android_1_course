package com.example.recycler_view_prj.presentation

import androidx.lifecycle.ViewModel
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import com.example.recycler_view_prj.data.PhotoDto
import com.example.recycler_view_prj.presentation.PhotoRecyclerView.PhotoPagingSource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.Flow
import java.util.*
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor() : ViewModel() {
    fun getPhotoList(dateView: Calendar, apiKey: String): Flow<PagingData<PhotoDto>> {
        return Pager(
            config = PagingConfig(pageSize = 10),
            pagingSourceFactory = { PhotoPagingSource(dateView, apiKey) }).flow
    }
}