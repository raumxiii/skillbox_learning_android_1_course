package com.example.recycler_view_prj.data

import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query


private const val BASE_URL = "https://api.nasa.gov/mars-photos/"

interface NasaMarsPhotoDataSource {
    @Headers(
        "Accept: application/json",
        "Content-type: application/json"
    )
    @GET("api/v1/rovers/curiosity/photos")
    suspend fun getPhotoList(
        @Query("earth_date") earthDateStr: String,
        @Query("api_key") apiKey: String,
        @Query("page") page: Int?
    ): Response<PhotoList>
}

object RetrofitInstance {
    private val retrofit =
        Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(MoshiConverterFactory.create())
            .build()
    val nasaMarsPhotoDataSource = retrofit.create(NasaMarsPhotoDataSource::class.java)
}