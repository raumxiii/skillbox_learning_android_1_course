package com.example.recycler_view_prj.entity

interface Rover {
    val id: Int
    val name: String
    val landingDateStr: String
    val launchDateStr: String
    val status: String
}