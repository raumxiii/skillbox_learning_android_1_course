package com.example.recycler_view_prj.entity

interface Camera {
    val id: String
    val name: String
    val roverId: Int
    val fullName: String
}