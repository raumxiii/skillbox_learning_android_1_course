package com.example.recycler_view_prj.domain

import com.example.recycler_view_prj.data.NasaMarsPhotoRepository
import com.example.recycler_view_prj.data.PhotoDto
import com.example.recycler_view_prj.entity.Photo
import java.util.*
import javax.inject.Inject

class GetNasaMarsPhotoListUseCase @Inject constructor(private val repository: NasaMarsPhotoRepository) {
    suspend fun execute(dateView: Calendar, apiKey: String, page: Int?): List<PhotoDto>? {
        return repository.getPhotoList(dateView, apiKey, page)
    }
}