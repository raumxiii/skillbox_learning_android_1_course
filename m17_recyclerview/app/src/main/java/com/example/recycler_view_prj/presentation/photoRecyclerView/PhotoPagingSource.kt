package com.example.recycler_view_prj.presentation.PhotoRecyclerView

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.example.recycler_view_prj.data.NasaMarsPhotoRepository
import com.example.recycler_view_prj.data.PhotoDto
import java.util.*

class PhotoPagingSource(private val dateView: Calendar, private val apiKey: String) :
    PagingSource<Int, PhotoDto>() {
    private val repository = NasaMarsPhotoRepository()
    override fun getRefreshKey(state: PagingState<Int, PhotoDto>): Int? = FIRST_PAGE

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, PhotoDto> {
        val page = params.key ?: FIRST_PAGE
        val calendar = Calendar.getInstance()
        calendar.set(2022, 6, 2)

        return kotlin.runCatching {
            repository.getPhotoList(dateView, apiKey, page = page)
        }.fold(onSuccess = {
            if (it != null)
                LoadResult.Page(
                    data = it,
                    prevKey = null,
                    nextKey = if (it.isEmpty()) null else page + 1
                )
            else
                LoadResult.Error(IllegalAccessException())
        },
            onFailure = { LoadResult.Error(it) })
    }

    private companion object {
        private const val FIRST_PAGE = 1
    }
}