package com.example.recycler_view_prj.presentation

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import dagger.hilt.android.AndroidEntryPoint
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.paging.LoadState
import androidx.paging.cachedIn
import com.example.recycler_view_prj.R
import com.example.recycler_view_prj.databinding.FragmentMainBinding
import com.example.recycler_view_prj.entity.Photo
import com.example.recycler_view_prj.presentation.PhotoRecyclerView.RecyclerPagedViewAdapter
import com.google.android.material.datepicker.CalendarConstraints
import com.google.android.material.datepicker.MaterialDatePicker
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import java.util.*

@AndroidEntryPoint
class MainFragment : Fragment() {

    private var _binding: FragmentMainBinding? = null
    private val binding get() = _binding!!
    private val viewDate = Calendar.getInstance()
    private var viewDateStr: String = CalendarConverter.getString(viewDate)
    private val viewModel: MainViewModel by viewModels()

    private val photoRecyclerPagedAdapter = RecyclerPagedViewAdapter { photo -> onItemClick(photo) }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentMainBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.viewModel = viewModel
        binding.lifecycleOwner = viewLifecycleOwner
        binding.dateViewTextView.text =
            "${resources.getString(R.string.date_view_prefix)} $viewDateStr"

        binding.dateViewTextView.setOnClickListener {
            val calendarConstraints =
                CalendarConstraints.Builder().setOpenAt(viewDate.timeInMillis).build()
            val getDateDialog = MaterialDatePicker.Builder
                .datePicker()
                .setTitleText(resources.getString(R.string.get_view_date_title))
                .setSelection(viewDate.timeInMillis)
                .setCalendarConstraints(calendarConstraints)
                .build()

            getDateDialog.addOnPositiveButtonClickListener { timeMillis ->
                viewDate.timeInMillis = timeMillis
                viewDateStr = CalendarConverter.getString(viewDate)
                binding.dateViewTextView.text =
                    "${resources.getString(R.string.date_view_prefix)} $viewDateStr"
            }
            getDateDialog.show(childFragmentManager, "viewDatePicker")
        }

        binding.loadPhotoButton.setOnClickListener {
            val apiKey = binding.apiKeyInputEditText.text.toString()
            viewModel.getPhotoList(viewDate, apiKey).cachedIn(viewLifecycleOwner.lifecycleScope)
                .onEach { photoRecyclerPagedAdapter.submitData(it) }
                .launchIn(viewLifecycleOwner.lifecycleScope)
        }

        binding.recycler.adapter = photoRecyclerPagedAdapter

        photoRecyclerPagedAdapter.loadStateFlow.onEach {
            when (it.refresh) {
                LoadState.Loading -> {
                    binding.loadPhotoButton.isEnabled = false
                    binding.loadingProgressBar.visibility = View.VISIBLE
                }
                else -> {
                    binding.loadPhotoButton.isEnabled = true
                    binding.loadingProgressBar.visibility = View.GONE
                }
            }
        }.launchIn(viewLifecycleOwner.lifecycleScope)
    }

    private fun onItemClick(item: Photo) {
        val bundle = Bundle().apply { putString("srcString", item.imgSrc) }
        findNavController().navigate(R.id.action_mainFragment_to_photoViewFragment, bundle)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}