package com.example.recycler_view_prj.data

import com.example.recycler_view_prj.entity.Photo
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class PhotoDto(
    @Json(name = "id") override val id: Int,
    @Json(name = "sol") override val sol: Int,
    @Json(name = "camera") override val camera: CameraDto,
    @Json(name = "img_src") override val imgSrc: String,
    @Json(name = "earth_date") override val earthDateStr: String,
    @Json(name = "rover") override val rover: RoverDto,
) : Photo