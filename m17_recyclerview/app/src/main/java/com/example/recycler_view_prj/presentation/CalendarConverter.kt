package com.example.recycler_view_prj.presentation

import java.text.SimpleDateFormat
import java.util.*

object CalendarConverter {
    private val dateFormat = SimpleDateFormat("yyyy-MM-dd")
    fun getCalendar(src: String): Calendar? {
        var result: Calendar? = null
        val calendar = Calendar.getInstance()
        try {
            calendar.time = dateFormat.parse(src)
            result = calendar
        } catch (e: Throwable) {
        }
        return result
    }

    fun getString(src: Calendar): String {
        return dateFormat.format(src.time)
    }
}